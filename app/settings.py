from logging.config import dictConfig
from pathlib import Path

from yaml import safe_load

project_root = Path(__file__).parent.parent

with open(project_root.joinpath("config.yml"), "r") as f:
    config = safe_load(f.read())

DATABASE_URL = config.get("DATABASE_URL", "postgres://ts:ts@pg:5432/ts")
URL: str = config["URL"]
ACCOUNTS: list = config.get("ACCOUNTS", [])
SERVER: dict = config.get("SERVER", {"host": "127.0.0.1", "port": 5001})

LOGGING_PATH = project_root.joinpath(config.get("LOGGING_CONFIG", "logging.yml"))
with open(LOGGING_PATH, "r") as f:
    logging_config = safe_load(f.read())
    dictConfig(logging_config)
