import asyncio
import logging
from functools import reduce
from operator import iconcat
from typing import List, Dict, Any, Type, Union

from asyncpg import DataError, UniqueViolationError
from pydantic import ValidationError
from xrpl.asyncio.account import get_account_payment_transactions
from xrpl.asyncio.clients import AsyncWebsocketClient
from xrpl.models import Subscribe, StreamParameter

from app.database import db
from app.settings import DATABASE_URL
from app.transaction.models import Transaction
from app.transaction.schema import (
    RawTransactionSchema,
    SubscriptionRawTransactionSchema,
)
from app.settings import URL, ACCOUNTS

logger = logging.getLogger(__name__)


async def transactions_handler(
    raw_transaction: dict,
    schema: Type[Union[RawTransactionSchema, SubscriptionRawTransactionSchema]],
):
    logger.debug(f"{raw_transaction=}")
    try:
        transaction = schema(**raw_transaction)
        result = await Transaction.create(**transaction.output())
        logger.debug(result)
    except (ValidationError, DataError, UniqueViolationError) as exc:
        logger.warning(exc)


async def fetch_account_transactions() -> List[Dict[str, Any]]:
    """
    Fetch transaction history for given accounts, return as a flat list
    """
    async with AsyncWebsocketClient(URL) as client:
        payment_transactions_tasks = (
            get_account_payment_transactions(account, client) for account in ACCOUNTS
        )
        payment_transactions = await asyncio.gather(*payment_transactions_tasks)
        return reduce(iconcat, payment_transactions, [])


def is_sufficient(message: dict) -> set:
    """
    Check that given message satisfies required account set
    """
    transaction = message.get("transaction", {})
    account = transaction.get("Account")
    destination = transaction.get("Destination")
    return set(ACCOUNTS) & {account, destination}


async def subscribe_account_transactions():
    async with AsyncWebsocketClient(URL) as client:
        await client.send(
            Subscribe(
                accounts=ACCOUNTS,
                streams=[StreamParameter.TRANSACTIONS],
            )
        )
        async for transaction in client:
            if is_sufficient(transaction):
                logger.debug(f"Sufficient {transaction=}")
                await transactions_handler(
                    transaction, SubscriptionRawTransactionSchema
                )


async def main():
    """
    Fetch accounts history then subscribe on new transactions
    """
    payment_transactions = await fetch_account_transactions()
    transactions_handler_tasks = (
        transactions_handler(payment_transaction, RawTransactionSchema)
        for payment_transaction in payment_transactions
    )
    async with db.with_bind(DATABASE_URL):
        await asyncio.gather(*transactions_handler_tasks)
        await subscribe_account_transactions()


if __name__ == "__main__":
    asyncio.run(main())
