import asyncio

from alembic import context
from sqlalchemy import create_engine

from app.settings import DATABASE_URL
from app.transaction.models import *

config = context.config
config.set_main_option(
    "sqlalchemy.url",
    DATABASE_URL,
)

target_metadata = db


async def run_migrations_online():
    connectable = create_engine(DATABASE_URL)
    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata)
        with context.begin_transaction():
            context.run_migrations()


asyncio.run(run_migrations_online())
