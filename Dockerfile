FROM library/python:3.10.2-buster AS builder

WORKDIR /var/log/transaction-service
WORKDIR /opt/transaction-service

ARG NEXUS_NETRC
ENV PATH="/venv/bin:$PATH"
ENV PYTHONUNBUFFERED=1

RUN python -m venv /venv
COPY requirements.txt /
RUN python -m pip install --upgrade pip && python -m pip install -r /requirements.txt --no-cache-dir

FROM library/python:3.10.2-buster AS app

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PATH="/venv/bin:$PATH"

WORKDIR /var/log/transaction-service
WORKDIR /opt/transaction-service

COPY --from=builder /venv /venv
COPY . .

COPY ./docker/docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["sh", "/docker-entrypoint.sh"]