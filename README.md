# Transaction Service

## Getting started
0) config.yml contains initial setup
1) docker-compose up -d --build
2) Server is listening on 127.0.0.1:80
3) /docs for swagger
4) Postgres instance is on :5437

## About 
1) service.py: runs API
2) subscriber.py: get accounts history at start, then subscribes on following transactions
