from aiohttp import web

from service import application
from app.settings import SERVER

if __name__ == "__main__":
    web.run_app(application(), **SERVER)
